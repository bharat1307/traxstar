//
//  main.m
//  TXTR
//
//  Created by chetna sisodia on 08/10/14.
//  Copyright (c) 2014 ___vvdn___. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TXTRAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TXTRAppDelegate class]));
    }
}
